import numpy as np
import h5py
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter



def verif_CV(c,i,j):
    fi = h5py.File("c="+str(c)+"/c="+str(c)+"ech"+str(i)+".hdf5","r")
    f=fi['fractions'][j]
    ChomEF=fi['ChomEF'][j]
    ChomHH=fi['ChomHH'][j]
    niter=len(ChomHH)
    iterations=np.zeros((niter,))
    C_11=np.zeros((niter,))
    C_22=np.zeros((niter,))
    C_12=np.zeros((niter,))
    C_33=np.zeros((niter,))
    C_11_EF=ChomEF[0,0]
    C_22_EF=ChomEF[1,1]
    C_12_EF=ChomEF[0,1]
    C_33_EF=ChomEF[2,2]/2
    for k in range(niter):
        iterations[k]=k+1
        C_11[k]=ChomHH[k,1,0,0]
        C_22[k]=ChomHH[k,1,1,1]
        C_12[k]=ChomHH[k,1,0,1]
        C_33[k]=ChomHH[k,1,2,2]/2

    plt.figure()
    g1,=plt.plot(iterations,C_11,color='red')
    plt.axhline(y=C_11_EF,color='red')
    g2,=plt.plot(iterations,C_22,color='blue')
    plt.axhline(y=C_22_EF,color='blue')
    g3,=plt.plot(iterations,C_12,color='yellow')
    plt.axhline(y=C_12_EF,color='yellow')
    g4,=plt.plot(iterations,C_33,color='green')
    plt.axhline(y=C_33_EF,color='green')
    plt.legend([g1,g2,g3,g4],['C_11','C_22','C_12','C_33'])
    plt.title('Contraste '+str(c)+' échantillon '+str(i)+' fraction '+str(f))
    plt.xlabel('itérations')
    plt.show()
    plt.close()

def verif_im(c,i):
    fi = h5py.File("c="+str(c)+"/c="+str(c)+"ech"+str(i)+".hdf5","r")
    images=fi['images']
    l=len(images)
    for j in range(l):
        f=fi['fractions'][j]
        plt.figure()
        plt.imshow(images[j,:,:],origin='lower')
        plt.savefig('c='+str(c)+'ech='+str(i)+'frac='+str(f)+'.pdf')
        plt.show()
        plt.close()


# for i in range(2):
#     for j in range(3):
#         verif_CV(100,i,j)

#verif_im(100,0)

def verif_eps(c,i,j,niter):
    fi = h5py.File("c="+str(c)+"/c="+str(c)+"ech"+str(i)+".hdf5","r")
    EpsHH=fi['EpsHH'][j,niter-1]
    frac=[0.05,0.1,0.2,0.3,0.4,0.5,0.6,0.8]
    f=frac[j]
    plt.figure()
    pp=plt.imshow(EpsHH[:,:,0,0],origin='lower')
    plt.colorbar(pp)
    plt.savefig('c='+str(c)+'/ech='+str(i)+'/frac='+str(f)+'/Aloc HH 11 iter='+str(niter)+'.pdf')
    #plt.show()
    plt.close()
    plt.figure()
    pp=plt.imshow(EpsHH[:,:,0,1],origin='lower')
    plt.colorbar(pp)
    plt.savefig('c='+str(c)+'/ech='+str(i)+'/frac='+str(f)+'/Aloc HH 12 iter='+str(niter)+'.pdf')
    #plt.show()
    plt.close()
    plt.figure()
    pp=plt.imshow(EpsHH[:,:,1,1],origin='lower')
    plt.colorbar(pp)
    plt.savefig('c='+str(c)+'/ech='+str(i)+'/frac='+str(f)+'/Aloc HH 22 iter='+str(niter)+'.pdf')
    #plt.show()
    plt.close()
    plt.figure()
    pp=plt.imshow(EpsHH[:,:,1,1],origin='lower')
    plt.colorbar(pp)
    plt.savefig('c='+str(c)+'/ech='+str(i)+'/frac='+str(f)+'/Aloc HH 33 iter='+str(niter)+'.pdf')
    #plt.show()
    plt.close()

#verif_eps(100,0,1,10)
#verif_eps(100,0,3,10)


for c in [100,0.01]:
    print("contraste "+str(c)+'\n')
    for i in range(10):
        print("échantillon "+str(i)+'\n')
        fi = h5py.File("c="+str(c)+"/c="+str(c)+"ech"+str(i)+".hdf5","r")
        fractions=fi['fractions']
        ChomEF=fi['ChomEF']
        ChomHH=fi['ChomHH']
        C_11=np.zeros((8,))
        C_11_EF=np.zeros((8,))
        C_22=np.zeros((8,))
        C_22_EF=np.zeros((8,))
        C_12=np.zeros((8,))
        C_12_EF=np.zeros((8,))
        C_33=np.zeros((8,))
        C_33_EF=np.zeros((8,))
        for j,f in enumerate([0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8]):
            C_11[j]=ChomHH[j,-1,1,0,0]/1.11111*np.sqrt(c)/10
            C_11_EF[j]=ChomEF[j,0,0]/1.11111*np.sqrt(c)/10
            C_22[j]=ChomHH[j,-1,1,1,1]/1.11111*np.sqrt(c)/10
            C_22_EF[j]=ChomEF[j,1,1]/1.11111*np.sqrt(c)/10
            C_12[j]=ChomHH[j,-1,1,0,1]/0.277777*np.sqrt(c)/10
            C_12_EF[j]=ChomEF[j,0,1]/0.277777*np.sqrt(c)/10
            C_33[j]=ChomHH[j,-1,1,2,2]/2/0.416666*np.sqrt(c)/10
            C_33_EF[j]=ChomEF[j,2,2]/2/0.416666*np.sqrt(c)/10
        plt.figure()
        g1,=plt.plot(fractions,C_11)
        g11,=plt.plot(fractions,C_11_EF)
        plt.legend([g1,g11],['HH','EF'])
        plt.title('C_11 contraste '+str(c)+' échantillon '+str(i))
        plt.xlabel('f')
        plt.ylabel('Cij/Cij_rigide')
        plt.axis([0,1,0,1])
        plt.show()
        plt.close()

        plt.figure()
        g1,=plt.plot(fractions,C_22)
        g11,=plt.plot(fractions,C_22_EF)
        plt.legend([g1,g11],['HH','EF'])
        plt.title('C_22 contraste '+str(c)+' échantillon '+str(i))
        plt.xlabel('f')
        plt.ylabel('Cij/Cij_rigide')
        plt.axis([0,1,0,1])
        plt.show()
        plt.close()

        plt.figure()
        g1,=plt.plot(fractions,C_12)
        g11,=plt.plot(fractions,C_12_EF)
        plt.legend([g1,g11],['HH','EF'])
        plt.title('C_12 contraste '+str(c)+' échantillon '+str(i))
        plt.xlabel('f')
        plt.ylabel('Cij/Cij_rigide')
        plt.axis([0,1,0,1])
        plt.show()
        plt.close()

        plt.figure()
        g1,=plt.plot(fractions,C_33)
        g11,=plt.plot(fractions,C_33_EF)
        plt.legend([g1,g11],['HH','EF'])
        plt.title('C_33 contraste '+str(c)+' échantillon '+str(i))
        plt.xlabel('f')
        plt.ylabel('Cij/Cij_rigide')
        plt.axis([0,1,0,1])
        plt.show()
        plt.close()




