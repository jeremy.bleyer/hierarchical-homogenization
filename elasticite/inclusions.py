import numpy as np
import matplotlib.pyplot as plt


from random import uniform


def compteur(im):
    nn,mm=im.shape
    cpt=0
    for i in range(nn):
        for j in range(mm):
            if im[i,j]==1:
                cpt=cpt+1
    return cpt/nn/mm
    
    
def ajoute(im,centers_diam,D,nb):
    nn,mm=im.shape
    ll=len(centers_diam)
    for i in range(nb):
        centers_diam.append([uniform(0,1),uniform(0,1),D])
    
    for i in range(nb):
        r=D/2
        x=centers_diam[ll+i][0]
        y=centers_diam[ll+i][1]    
        for j in range(int((x-r)*nn)-1,int((x+r)*nn)+1):
            for k in range(int((y-r)*mm)-1,int((y+r)*mm)+1):                
                x1=(j+0.5)/nn
                y1=(k+0.5)/mm
                if (x1-x)**2+(y1-y)**2<=r**2:
                    if j>=nn:
                        l=nn
                    else:
                        l=0
                    if k>=mm:
                        m=mm
                    else:
                        m=0
                    im[j-l,k-m]=1
    
def enleve(im,centers_diam,nb):
    nn,mm=im.shape
    im2=np.zeros((nn,mm))
    
    for i in range(nb):
        centers_diam.pop()
        
    ll=len(centers_diam)
    for i in range(ll):
        r=centers_diam[i][2]/2
        x=centers_diam[i][0]
        y=centers_diam[i][1]    
        for j in range(int((x-r)*nn)-1,int((x+r)*nn)+1):
            for k in range(int((y-r)*mm)-1,int((y+r)*mm)+1):                
                x1=(j+0.5)/nn
                y1=(k+0.5)/mm
                if (x1-x)**2+(y1-y)**2<=r**2:
                    if j>=nn:
                        l=nn
                    else:
                        l=0
                    if k>=mm:
                        m=mm
                    else:
                        m=0
                    im2[j-l,k-m]=1
    
    return im2
    
    

def image_init(N,D,f):

    im=np.zeros((N,N))
    
    n=int(f/np.pi/(D/2)**2)
    
    centers_diam=[]    
    c=0
    while c<f:
        ajoute(im,centers_diam,D,n)
        c=compteur(im)
        n=int((f-c)/np.pi/(D/2)**2)+1

    return im,centers_diam,c
    

def image_augmente(im,centers_diam,D,f):
    f0=compteur(im)
    if f<=f0:
        print('fraction volumique actuelle :'+str(f0))
    else:
        c=f0
        n=int((f-c)/np.pi/(D/2)**2)+1
        while c<f:
            ajoute(im,centers_diam,D,n)
            c=compteur(im)
            n=int((f-c)/np.pi/(D/2)**2)+1
        
        return c
        
def image_diminue(im,centers_diam,f):
    f0=compteur(im)
    if f>f0:
        print('fraction volumique actuelle :'+str(f0))
    else:
        c=f0
        im2=im
        while c>f:
            l=len(centers_diam)
            cc=0.
            i=0
            while cc < c-f:
                r=centers_diam[l-i-1][2]/2
                cc+=np.pi*r**2 
                i+=1
            im2=enleve(im2,centers_diam,i)
            c=compteur(im2)
        nn,mm=im.shape
        for i in range(nn):
            for j in range(mm):
                im[i,j]=im2[i,j]
        return c
            


    



