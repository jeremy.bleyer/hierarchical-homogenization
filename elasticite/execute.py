#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
import h5py
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from Reference import reference
from Homog_hierar import homog_hierar
from inclusions import image_init,image_augmente,image_diminue
import os


N=256
D=0.03


C=[0. for i in range(2)]

E=[0.,0.]
nu=[0.,0.]

E[0]=1
nu[0]=0.2
nu[1]=0.2


for c in [100,0.01]:
    print("contraste "+str(c)+'\n')
    os.system("mkdir c="+str(c))
    E[1]=1/c
    for i in range(2):
        C[i]=np.array([[(1-nu[i])*E[i]/(1-2*nu[i])/(1+nu[i]),
                        nu[i]*E[i]/(1-2*nu[i])/(1+nu[i]),0.],
                       [nu[i]*E[i]/(1-2*nu[i])/(1+nu[i]),(1-nu[i])*E[i]/(1-2*nu[i])/(1+nu[i]),0.],
                       [0.,0.,E[i]/(1+nu[i])]])

    for i in range(10):
        os.system("mkdir c="+str(c)+"/ech="+str(i))
        print("échantillon "+str(i)+'\n')
        fname = "c="+str(c)+"/c="+str(c)+"ech"+str(i)+".hdf5"
        fi = h5py.File(fname, "w")
        im,centers_diam,f0=image_init(N,D,0.9)
        l=len(centers_diam)
        fi['centers']=np.zeros((8,l,3))
        fi['images']=np.zeros((8,N,N))
        fi['fractions']=np.zeros((8,))
        fi['ChomEF']=np.zeros((8,3,3))
        fi['CVoigt']=np.zeros((8,3,3))
        fi['ChomHH']=np.zeros((8,10,2,3,3))
        fi['EpsHH']=np.zeros((8,10,N,N,3,3))

        for j,f in enumerate([0.8,0.6,0.5,0.4,0.3,0.2,0.1,0.05]):
            os.system("mkdir c="+str(c)+"/ech="+str(i)+"/frac="+str(f))
            print("fraction volumique "+str(f)+'\n')
            f1=image_diminue(im,centers_diam,f)
            # plt.figure()
            # plt.imshow(im[:,:],origin='lower')
            # plt.show()
            # plt.close()
            fi['images'][7-j]=im
            l=len(centers_diam)
            for ii in range(l):
                fi['centers'][7-j][ii]=centers_diam[ii]
            fi['fractions'][7-j]=f1

            tC=np.array([[C[0] for ii in range(N)] for jj in range(N)])
            for ii in range(N):
                for jj in range(N):
                    if im[ii,jj]==1:
                        tC[ii,jj]=C[1]

            ChomEF,CVoigt = reference(tC,256,c,i,f)
            fi['ChomEF'][7-j]=ChomEF
            fi['CVoigt'][7-j]=CVoigt

            ChomHH_list,tA_list=homog_hierar(tC,CVoigt)
            l=len(ChomHH_list)
            if l<10:
                for k in range(l):
                    fi['ChomHH'][7-j,k]=ChomHH_list[k]
                    fi['EpsHH'][7-j,k]=tA_list[k]
                for k in range(l,10):
                    fi['ChomHH'][7-j,k]=ChomHH_list[l-1]
                    fi['EpsHH'][7-j,k]=tA_list[l-1]
            else:
                for k in range(10):
                    fi['ChomHH'][7-j,k]=ChomHH_list[k]
                    fi['EpsHH'][7-j,k]=tA_list[k]
