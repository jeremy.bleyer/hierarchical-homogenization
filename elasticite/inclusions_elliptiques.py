import numpy as np
import matplotlib.pyplot as plt


from random import uniform


def compteur(im):
    nn,mm=im.shape
    cpt=0
    for i in range(nn):
        for j in range(mm):
            if im[i,j]==1:
                cpt=cpt+1
    return cpt/nn/mm
    
    
def ajoute(im,centers,a,b,th,iso,nb):
    nn,mm=im.shape
    ll=len(centers)
    for i in range(nb):
        if iso==True:
            centers.append([uniform(0,1),uniform(0,1),uniform(0,np.pi),a,b])
        else:
            centers.append([uniform(0,1),uniform(0,1),th,a,b])    

    for i in range(nb):
        c=max(centers[ll+i][3],centers[ll+i][4])
        x=centers[ll+i][0]
        y=centers[ll+i][1]  
        the=centers[ll+i][2]  
        for j in range(int((x-c)*nn)-1,int((x+c)*nn)+1):
            for k in range(int((y-c)*mm)-1,int((y+c)*mm)+1):                
                x1=(j+0.5)/nn
                y1=(k+0.5)/mm
                x2=(x1-x)*np.cos(the)+(y1-y)*np.sin(the)
                y2=(y1-y)*np.cos(the)-(x1-x)*np.sin(the)
                if x2**2/a**2+y2**2/b**2<=1:
                    if j>=nn:
                        l=nn
                    else:
                        l=0
                    if k>=mm:
                        m=mm
                    else:
                        m=0
                    im[j-l,k-m]=1
                    
def enleve(im,centers,nb):
    nn,mm=im.shape
    im2=np.zeros((nn,mm))
    
    for i in range(nb):
        centers.pop()
        
    ll=len(centers)
    for i in range(ll):
        a=centers[i][3]
        b=centers[i][4]
        c=max(a,b)
        x=centers[i][0]
        y=centers[i][1]  
        the=centers[i][2]  
        for j in range(int((x-c)*nn)-1,int((x+c)*nn)+1):
            for k in range(int((y-c)*mm)-1,int((y+c)*mm)+1):                
                x1=(j+0.5)/nn
                y1=(k+0.5)/mm
                x2=(x1-x)*np.cos(the)+(y1-y)*np.sin(the)
                y2=(y1-y)*np.cos(the)-(x1-x)*np.sin(the)
                if x2**2/a**2+y2**2/b**2<=1:
                    if j>=nn:
                        l=nn
                    else:
                        l=0
                    if k>=mm:
                        m=mm
                    else:
                        m=0
                    im2[j-l,k-m]=1
    
    return im2
    
    

def image_init(N,a,b,th,iso,f):

    im=np.zeros((N,N))
    
    n=int(f/np.pi/a/b)
    centers=[]
    c=0
    while c<f:
        ajoute(im,centers,a,b,th,iso,n)
        c=compteur(im)
        n=int((f-c)/np.pi/a/b)+1

    return im,centers,c
    

def image_augmente(im,centers,a,b,th,iso,f):
    f0=compteur(im)
    if f<=f0:
        print('fraction volumique actuelle :'+str(f0))
    else:
        c=f0
        n=int((f-c)/np.pi/a/b)+1
        while c<f:
            ajoute(im,centers,a,b,th,iso,n)
            c=compteur(im)
            n=int((f-c)/np.pi/a/b)+1
        
        return c
        
        
def image_diminue(im,centers,f):
    f0=compteur(im)
    if f>f0:
        print('fraction volumique actuelle :'+str(f0))
    else:
        c=f0
        im2=im
        while c>f:
            l=len(centers)
            cc=0.
            i=0
            while cc < c-f:
                a=centers[l-i-1][3]
                b=centers[l-i-1][4]
                cc+=np.pi*a*b
                i+=1
            im2=enleve(im2,centers,i)
            c=compteur(im2)
        nn,mm=im.shape
        for i in range(nn):
            for j in range(mm):
                im[i,j]=im2[i,j]
        return c
        

im,centers,c=image_init(256,0.02,0.05,0.3,False,0.3)
plt.figure()
plt.imshow(im[:,:])
plt.savefig('1.pdf')
plt.show()
plt.close()
image_augmente(im,centers,0.01,0.06,0.,True,0.5)
plt.figure()
plt.imshow(im[:,:])
plt.savefig('2.pdf')
plt.show()
plt.close()
image_diminue(im,centers,0.2)
plt.figure()
plt.imshow(im[:,:])
plt.savefig('3.pdf')
plt.show()
plt.close()
            


    



