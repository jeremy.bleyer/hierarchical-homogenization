import numpy as np
from numba import jit, float64, int64, prange
import matplotlib.pyplot as plt
import time
from scipy.optimize import fsolve

from greenOperatorConductionNumba import *
#from discretizationGrid import *
#from constitutiveLaw import *
from microstructure import *

@jit
def nullifyField(field,N):
    #z=np.zeros(d)
    for n in np.ndindex(N):
        field[n] = np.zeros(len(N))

def getLocalGreenNumba(M,k0):
    d=len(M)
    filter_level=2
    field,field_fourier,fft,ifft,tupleK,frequencies,filters,tupleFilters = initializeGreen(M,filter_level=filter_level)
    gammaLocal = np.zeros(tuple(d*[3] + 2*[d]))
    for i in range(d):
        nullifyField(field,tuple(M))
        field[tuple(d*[0]+[i])] = 1
        operate_field(field_fourier,field_fourier,fft,ifft,tupleK,M,frequencies,filter_level,filters,tupleFilters,k0)
        for m in np.ndindex(tuple(d*[3])):
            s = tuple([m[j]-1 for j in range(d)])
            gammaLocal[s][i]+=field[s]
    return gammaLocal
    
"""
def getLocalGreen(M,k0):
    d = len(M)
    grid = discretizationGrid(M)
    gamma = greenOperatorConduction(grid,k0,discretization="truncated",filter_level=2)
    gammaLocal = np.zeros(tuple(d*[3] + 2*[gamma.numComp]))
    for i in range(gamma.numComp):
        for n in np.ndindex(tuple(M)):
            gamma.field[n] = np.zeros(d)
        gamma.field[tuple(d*[0]+[i])] = 1
        gamma.operate_field()
        for m in np.ndindex(tuple(d*[3])):
            s = tuple([m[j]-1 for j in range(d)])
            gammaLocal[s][i]+=gamma.field[s]
    return gammaLocal
"""

def getGreenMatrix(M,k0,gammaLocal,shifts):
    d=len(M)
    numDOF = d*2**d
    matrix = np.zeros((numDOF,numDOF))
    i = 0
    for I in np.ndindex(shifts):
        j = 0
        for J in np.ndindex(shifts):
            shift=tuple(np.array(I)-np.array(J))
            matrix[d*i:d*(i+1),d*j:d*(j+1)] -= gammaLocal[shift]
            j+=1
        i+=1
    return matrix

def resetReference(N,k0,greenMatrices,phaseProps,phaseInvDeltaProp):
    M=2*np.ones(N.shape,dtype=np.int64)
    if k0.__class__ == np.ndarray:
        refProp = k0
    else:
        refProp = k0*np.eye(len(N))
    for i in range(len(phaseProps)):
        kloc=phaseProps[i]
        phaseInvDeltaProp[i] = np.linalg.inv(np.eye(d)*kloc-refProp)
    depth = 0
    shifts = tuple(d*[2])
    while M[0]<=N[0]:
        gammaLocal = getLocalGreenNumba(M,k0)
        greenMatrices[depth] = getGreenMatrix(M,k0,gammaLocal,shifts)
        M*=2
        depth+=1
    return(refProp)

@jit(nopython=True)#(float64[:,:](int64,float64[:,:],float64[:,:],float64[:,:]))
def getEquivalentProperty(d, localProperties, refProp, greenMatrix):
    #Tk = array of tensors obtained by inversion of HS matrix 
    #Ck-1 = Cref + avg(Tk):inv(avg(Mk:Tk))
    matrix = localProperties+greenMatrix
    #print(matrix)
    numDOF = len(matrix)#d*2**d
    avgT = np.zeros((d,d),dtype=np.float64)
    avgMT = np.zeros((d,d),dtype=np.float64)
    rhs = np.zeros(numDOF,dtype=np.float64)
    for i in range(d):
        macroGradient = np.array([1. if i==j else 0. for j in range(d)],dtype=np.float64)
        for cell in range(2**d):
            rhs[d*cell:d*(cell+1)] = macroGradient
        tau = np.linalg.solve(matrix,rhs)
        eps = localProperties.dot(tau)
        for cell in range(2**d):
            avgT[i]+=tau[d*cell:d*(cell+1)]
            avgMT[i]+=eps[d*cell:d*(cell+1)]
        avgT[i]/=2**d
        avgMT[i]/=2**d
    return avgMT.dot(np.linalg.inv(avgT))#refProp+avgT.dot(np.linalg.inv(avgMT))


@jit(nopython=True)#(float64[:,:](int64,int64[:],int64[:],int64,float64[:,:],float64[:],float64[:,:],float64[:,:,:]))
def recursion(depth, M, pshifts, position, stride, phasemap, mapStrides, phaseProps, refProp, greenMatrices):
    d=len(M)
    localProperties = np.zeros(greenMatrices[0].shape)
    #i=0
    #for shift in np.ndindex(shifts):#tuple(d*[2])):
    for i in prange(2**d):
        nextPosition = position+stride*pshifts[i]#np.array(shift)
        if stride > 1:
            #localProperties[d*i:d*(i+1),d*i:d*(i+1)] = np.linalg.inv(recursion(depth+1,M*2,shifts,nextPosition,stride/2, phasemap, mapStrides, phaseProps, refProp, greenMatrices)-refProp)
            localProperties[d*i:d*(i+1),d*i:d*(i+1)] = recursion(depth+1,M*2,pshifts,nextPosition,stride/2, phasemap, mapStrides, phaseProps, refProp, greenMatrices)
        else:
            n = position+stride*pshifts[i]#np.array(shift) #tuple() constructor not supported by numba
            nflat = np.sum(n*mapStrides)
            #kloc=phaseProps[phasemap[nflat]] 
            #localProperties[d*i:d*(i+1),d*i:d*(i+1)] = np.linalg.inv(np.eye(d)*kloc-refProp)
            localProperties[d*i:d*(i+1),d*i:d*(i+1)] = phaseProps[phasemap[nflat]] 
    #    i+=1
    equivalentProperty = getEquivalentProperty(d, localProperties, refProp, greenMatrices[depth])
    return equivalentProperty

@jit(nopython=True,parallel=False)#very minor speed up, communication problem ?
def recursionParallel(depth, M, pshifts, position, stride, phasemap, mapStrides, phaseProps, refProp, greenMatrices):
    d=len(M)
    numDOF = d*2**d
    localProperties = np.zeros((numDOF,numDOF))#np.zeros(greenMatrices[0].shape)
    for i in prange(2**d):
        nextPosition = position+stride*pshifts[i]
        localProperties[d*i:d*(i+1),d*i:d*(i+1)] = recursion(depth+1,M*2,pshifts,nextPosition,stride/2, phasemap, mapStrides, phaseProps, refProp, greenMatrices)#do not use tuple as arguments in parallel mode
    equivalentProperty = getEquivalentProperty(d, localProperties, refProp, greenMatrices[depth])
    return equivalentProperty

def hierachicHomogenize(N,phasemap,phaseProps):
    d=len(N)
    phaseInvDeltaProp = np.zeros((len(phaseProps),d,d))
    numDOF = d*2**d
    greenMatrices=np.zeros((int(np.log2(N[0])),numDOF,numDOF),dtype=np.float64)
    shifts = tuple(d*[2])
    pshifts = np.zeros((d**2,d),dtype=np.int64)
    i=0
    for shift in np.ndindex(shifts):
        pshifts[i] += np.array(shift)
        i+=1
    k0=np.mean(phaseProps)*np.eye(len(N))
    refProp = resetReference(N,k0,greenMatrices,phaseProps,phaseInvDeltaProp)
    print("delta\treset(s)\trecurrence(s)")
    delta = 1
    while delta > 1e-3:
        M=2*np.ones(N.shape,dtype=np.int64)
        depth = 0
        position=np.zeros(N.shape,dtype=np.int64)
        stride = N0/2
        start = time.time()    
        invdk = recursionParallel(depth, M, pshifts, position, stride, phasemap, mapStrides, phaseInvDeltaProp, refProp, greenMatrices)
        kest = refProp+np.linalg.inv(invdk)
        end = time.time()
        recursionTime = end - start
        prevk0 = k0
        k0 = (kest+kest.transpose())/2#np.diag(np.diag(kest))
        delta = np.linalg.norm(k0-prevk0)/np.linalg.norm(k0)
        start = time.time()
        #refProp = resetReference(N,k0,greenMatrices)
        refProp = resetReference(N,k0,greenMatrices,phaseProps,phaseInvDeltaProp)
        end = time.time()
        resetTime = end-start
        #print(k0)
        print("%e\t%s\t%s" % (delta,resetTime,recursionTime))
    return kest
    print(kest)
    khom.append(kest)


    phi=np.sum(phasemap)*1./N0**d
    frac.append(phi)
    print("voigt %f" % ((1-phi)*k1+phi*k2))
    print("reuss %f" % (1./((1-phi)/k1+phi/k2)))
    



N0 = 128
d=3
N=N0*np.ones(d,dtype=np.int64)
radius = 8
shape = {2:"disc",3:"sphere"}
V = pi*radius**2 if d==2 else 4*pi/3*radius**3
k1 = 1.
k2 = 0.001
Khom = []
Frac = []
for model in ["A","B"]:
    if model == "A":
        phaseProps = np.array([k1,k2],dtype=np.float64)
    elif model == "B":
        phaseProps = np.array([k2,k1],dtype=np.float64)
    khom=[]
    frac=[]
    for target_phi in range(75,99,4):
        ncenter = int(-np.log(1-target_phi/100.)*N0**d/V)
        centers = np.random.rand(ncenter,d)*N0
        radii = np.ones(ncenter)*radius
        microstructure = booleanSpheres(centers,radii,N)
        #microstructure = booleanSpheres(np.array([[0,0],[N0/3,N0/3],[3*N0/4,N0/2]]),[N0/3,N0/3,N0/3],N)
        mapStrides = np.array([N0**(d-i-1) for i in range(d)],dtype=np.int64)
        phasemap = microstructure.phasemap.flatten() #TODO : flatten to allow scalar index (see numba issue with tuple index)
        phi=np.sum(phasemap)*1./N0**d
        frac.append(phi)
        print("target phi %f, achieved phi %f, num. centers %d" % (target_phi/100.,phi,ncenter))
        #microstructure = square(N0/2)
        """
        plt.figure()
        plt.imshow(microstructure.phasemap, cmap="Greys")
        plt.show(block=False)
        """
        khh = hierachicHomogenize(N,phasemap,phaseProps)
        print(khh)
        khom.append(khh)
        print("voigt %f" % ((1-phi)*k1+phi*k2))
        print("reuss %f" % (1./((1-phi)/k1+phi/k2)))
    Khom.append(np.array(khom))
    Frac.append(np.array(frac))
    savefile=open("boolean_%s_contrast%dmodel%s.txt" % (shape[d],int(k1/k2),model), 'ab')
    np.savetxt(savefile,np.array([[f,np.trace(kh)/d] for (f,kh) in zip(frac,khom)]))
    savefile.close()

Kmoritanaka = lambda x,k1,k2,d : ((1-x)*k1+x*k2*d*k1/((d-1)*k1+k2))/((1-x)+x*d*k1/((d-1)*k1+k2))
eqKselfconsistent = lambda k,x,k1,k2,d : 1e6 if k <0 else ((1-x)*(k1-k)*k*d/((d-1)*k+k1)+x*(k2-k)*k*d/((d-1)*k+k2))
Kselfconsistent= lambda x,k1,k2,d : fsolve(eqKselfconsistent,(k1+k2)/2,args=(x,k1,k2,d))

x=np.linspace(0,1,101)
plt.plot(x,Kmoritanaka(x,k1,k2,d),label="Hashin-Shtrikman")
plt.plot(x,Kmoritanaka(1-x,k2,k1,d),label="Hashin-Shtrikman")
plt.plot(x,[Kselfconsistent(f2,k1,k2,d) for f2 in x],label="Self-Consistent")
resA = np.loadtxt("boolean_%s_contrast%dmodel%s.txt" % (shape[d],int(k1/k2),"A"))
plt.plot(resA[:,0],resA[:,1],'k.',label="hierachical num. self-consistent A")
resB = np.loadtxt("boolean_%s_contrast%dmodel%s.txt" % (shape[d],int(k1/k2),"B"))
plt.plot(1-resB[:,0],resB[:,1],'k+',label="hierachical num. self-consistent B")
plt.legend(loc="upper right")
plt.title(r"boolean %s contrast = %d" % (shape[d],int(k1/k2)))
plt.xlabel("volume fraction of insulating phase")
plt.ylabel(r"$K_{hom} / K_{conductive \, phase}$")
plt.savefig("boolean_%s_contrast%d.pdf" % (shape[d],int(k1/k2)))
plt.show()

