from numba import jitclass,int16,int8,jit
import numpy as np

#@jitclass
#class microstructure():
#    def __init__(self):
#        pass
#
#    def get_phase(self):
#        pass
    
@jitclass([('a',int16)])
class square(object):
    def __init__(self,a):
        self.a=a
        
    def get_phase(self,n):
        for ni in n:
            if ni>=self.a:
                return 0
        return 1

class booleanSpheres(object):
    def __init__(self,centers,radii,N):
        self.centers=centers
        self.radii=radii
        self.N = N
        self.d = len(N)
        self.phasemap = np.zeros(tuple(N),dtype=np.int8)
        self.initializePhaseMap()

    def initializePhaseMap(self):
        for i,center in enumerate(self.centers):
            r = self.radii[i]
            c = np.rint(center).astype(int)
            for s in np.ndindex(tuple(self.d*[2*int(r)+1])):
                ds = np.array(s)-int(r)
                n = c-ds
                if (n-center+0.5).dot(n-center+0.5)<= r*r:
                    self.phasemap[tuple(n % self.N)]=1
        
    def get_phase(self,n):
        return self.phasemap[n]

    
